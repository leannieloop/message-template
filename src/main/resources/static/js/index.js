$(document).ready(function () {
    loadGuests();
    loadCompanies();
    loadMessages();
    $('#message-form').submit(function () {
        var isValid = false;
        if (!$('#guest-list').val()) {
            alert('Select a guest.');
        } else if (!$('#company-list').val()) {
            alert('Select a company.');
        } else if (!$('#message-list').val()) {
            alert('Select a message template.');
        } else {
            isValid = true;
        }
        if (isValid) {
            sendMessage();
        }
        return false;
    });
    $('#add-placeholder').click(function () {
        var isSelected = false;
        var placeholder = $('input[name="placeholders"]:checked').val();
        if (!placeholder) {
            alert('Select a placeholder to add it to the message template.');
        } else {
            isSelected = true;
        }
        if (isSelected) {
            var text = $('#message-body').val() + placeholder;
            $('#message-body').val(text);
        }
    });
    $('#reset-template').click(function () {
        $('#message-body').empty();
    });
    $('#template-form').submit(function () {
        var isValid = false;
        if (!$('#title').val().trim()) {
            alert('Enter a title.');
        } else if (!$('#message-body').val().trim()) {
            alert('Enter a message.');
        } else {
            isValid = true;
        }
        if (isValid) {
            saveTemplate();
        }
        return false;
    });
});

var guests = [];
var companies = [];
var messages = [];

function loadGuests() {
    $('#guest-list').empty()
            .append('<option value="" disabled selected>Choose a Guest</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/guests',
        success: function (data, status) {
            $.each(data, function (index, guest) {
                guests.push(guest);
                var i = guests.indexOf(guest);
                $('#guest-list').append('<option value="' + i + '">'
                        + guests[i].firstName + ' ' + guests[i].lastName + '</option>');
            });
            $('#guest-list').material_select();
        },
        error: function () {
            $('#guest-load-error').show();
        }
    });
}

function loadCompanies() {
    $('#company-list').empty()
            .append('<option value="" disabled selected>Choose a Company</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/companies',
        success: function (data, status) {
            $.each(data, function (index, company) {
                companies.push(company);
                var i = companies.indexOf(company);
                $('#company-list').append('<option value="' + i + '">'
                        + companies[i].company + ', ' + companies[i].city + '</option>');
            });
            $('#company-list').material_select();
        },
        error: function () {
            $('#company-load-error').show();
        }
    });
}

function loadMessages() {
    $('#message-list').empty()
            .append('<option value="" disabled selected>Choose a Message</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/messages',
        success: function (data, status) {
            $.each(data, function (index, message) {
                messages.push(message);
                var i = messages.indexOf(message);
                $('#message-list').append('<option value="' + i + '">'
                        + messages[i].title + '</option>');
            });
            $('#message-list').material_select();
        },
        error: function () {
            $('#message-load-error').show();
        }
    });
}

function sendMessage() {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/message',
        data: JSON.stringify({
            guest: guests[$('#guest-list').val()],
            company: companies[$('#company-list').val()],
            message: messages[$('#message-list').val()]
        }),
        headers: {
            'Content-Type': 'application/json'
        },
        success: function (message) {
            $('div#message-viewer').empty().append(
                    '<div class="card-panel deep-purple"><span class="white-text">'
                    + message + '</span></div>');
        },
        error: function () {
            Materialize.toast('Unable to send message. Please try again later.',
                    3000, 'rounded');
        }
    });
}

function saveTemplate() {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/messagetemplate',
        data: JSON.stringify({
            title: $('#title').val().trim(),
            template: $('#message-body').val().trim()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function () {
            Materialize.toast('Template successfully saved!', 3000, 'rounded');
            $('#template-form')[0].reset();
            $('#message-body').empty();
            loadMessages();
        },
        error: function () {
            Materialize.toast('Unable to save template. Please try again later.',
                    3000, 'rounded');
        }
    });
}