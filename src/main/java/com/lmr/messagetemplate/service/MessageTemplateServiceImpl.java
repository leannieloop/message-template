package com.lmr.messagetemplate.service;

import com.lmr.messagetemplate.client.TextMessageClient;
import com.lmr.messagetemplate.dao.CompanyDao;
import com.lmr.messagetemplate.dao.GuestReservationDao;
import com.lmr.messagetemplate.dao.MessageDao;
import com.lmr.messagetemplate.domain.Company;
import com.lmr.messagetemplate.domain.Guest;
import com.lmr.messagetemplate.domain.Message;
import com.lmr.messagetemplate.domain.MessageContext;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageTemplateServiceImpl implements MessageTemplateService {
    @Autowired
    private GuestReservationDao guestResDao;
    @Autowired
    private CompanyDao companyDao;
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private TextMessageClient client;

    @Override
    public List<Guest> getAllGuests() {
        return guestResDao.getAllGuests();
    }

    @Override
    public List<Company> getAllCompanies() {
        return companyDao.getAllCompanies();
    }

    @Override
    public List<Message> getAllMessages() {
        return messageDao.getAllMessages();
    }

    @Override
    public String sendMessage(MessageContext msgCtx) {
        Map<String, Object> placeholders = getSelectedPlaceholders(msgCtx);
        String message = msgCtx.getMessage().getTemplate();
        Matcher matcher = Pattern.compile("\\$\\{(.*?)\\}").matcher(message);
        while (matcher.find()) {
            message = message.replace(matcher.group(), placeholders.get(matcher.group()).toString());
        }
        return client.sendMessage(message);
    }
    
    private Map<String, Object> getSelectedPlaceholders(MessageContext msgCtx) {
        ZoneId timezone = ZoneId.of(msgCtx.getCompany().getTimezone());
        
        Map<String, Object> placeholders = new HashMap<>();
        
        placeholders.put("${greeting}", getGreeting(timezone));
        placeholders.put("${company}", msgCtx.getCompany().getCompany());
        placeholders.put("${city}", msgCtx.getCompany().getCity());
        placeholders.put("${roomNumber}", msgCtx.getGuest().getReservation().getRoomNumber());
        placeholders.put("${startTimestamp}", 
                getLocalTime(timezone, msgCtx.getGuest().getReservation().getStartTimestamp()));
        placeholders.put("${endTimestamp}",
                getLocalTime(timezone, msgCtx.getGuest().getReservation().getEndTimestamp()));
        placeholders.put("${firstName}", msgCtx.getGuest().getFirstName());
        placeholders.put("${lastName}", msgCtx.getGuest().getLastName());
        return placeholders;
    }

    private String getGreeting(ZoneId timezone) {
        LocalTime timeNow = LocalTime.now(timezone);
        if (timeNow.getHour() < 12) {
            return "Good morning";
        } else if (timeNow.getHour() >= 12 && timeNow.getHour() < 18) {
            return "Good afternoon";
        } else {
            return "Good evening";
        }
    }

    private LocalTime getLocalTime(ZoneId timezone, Long timestamp) {
        return LocalDateTime.ofInstant(
                Instant.ofEpochMilli(timestamp), timezone)
                .toLocalTime()
                .truncatedTo(ChronoUnit.MINUTES);
    }
    
    @Override
    public void createMessageTemplate(Message message) {
        List<Message> messages = messageDao.getAllMessages()
                .stream()
                .sorted(Comparator.comparing(Message::getId))
                .collect(Collectors.toList());
        message.setId(messages.get(messages.size() - 1).getId() + 1);
        messageDao.createMessageTemplate(message);
    }
}
