package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Message;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

@Repository
public class MessageDaoImpl implements MessageDao {
    private final String JSON_FILE = "Messages.json";
    private List<Message> messages = new ArrayList<>();

    @Override
    public List<Message> getAllMessages() {
        messages.clear();
        readMessagesJsonFile();
        return messages;
    }
    
    private void readMessagesJsonFile() {
        JSONParser parser = new JSONParser();
        
        try {
            JSONArray messageFile = (JSONArray) parser.parse(
                    new FileReader(JSON_FILE));
            for (Object messageObject : messageFile) {
                JSONObject messageJson = (JSONObject) messageObject;
                Long id = (Long) messageJson.get("id");
                String title = (String) messageJson.get("title");
                String template = (String) messageJson.get("template");
                Message message = new Message(id, title, template);
                messages.add(message);
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void createMessageTemplate(Message message) {
        messages.add(message);
        writeMessagesJsonFile();
    }
    
    private void writeMessagesJsonFile() {
        JSONArray messagesArray = new JSONArray();
        for (Message message : messages) {
            JSONObject messageObject = new JSONObject();
            messageObject.put("id", message.getId());
            messageObject.put("title", message.getTitle());
            messageObject.put("template", message.getTemplate());
            messagesArray.add(messageObject);
        }
        try {
            FileWriter file = new FileWriter(JSON_FILE);
            file.write(messagesArray.toJSONString());
            file.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
