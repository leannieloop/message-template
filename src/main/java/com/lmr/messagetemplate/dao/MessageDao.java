package com.lmr.messagetemplate.dao;

import com.lmr.messagetemplate.domain.Message;
import java.util.List;

public interface MessageDao {
    List<Message> getAllMessages();
    void createMessageTemplate(Message message);
}
