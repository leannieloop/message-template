package com.lmr.messagetemplate.controller;

import com.lmr.messagetemplate.domain.Guest;
import com.lmr.messagetemplate.service.MessageTemplateService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GuestController {
    @Autowired
    private MessageTemplateService service;
    
    @RequestMapping(value = "/guests", method = RequestMethod.GET)
    @ResponseBody
    public List<Guest> getAllGuests() {
        return service.getAllGuests();
    }
}
