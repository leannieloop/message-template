package com.lmr.messagetemplate.controller;

import com.lmr.messagetemplate.domain.Message;
import com.lmr.messagetemplate.domain.MessageContext;
import com.lmr.messagetemplate.service.MessageTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
public class MessageController {
    @Autowired
    private MessageTemplateService service;

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    @ResponseBody
    public List<Message> getAllMessages() {
        return service.getAllMessages();
    }
    
    @RequestMapping(value = "/message", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String sendMessage(@RequestBody MessageContext msgCtx) {
        return service.sendMessage(msgCtx);
    }
    
    @RequestMapping(value = "/messagetemplate", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createMessageTemplate(@RequestBody Message message) {
        service.createMessageTemplate(message);
    }
}
